﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Lab3
{
    public class MagazineCollection
    {
        private readonly List<Magazine> _magazines = new List<Magazine>();

        public double MaxRating => _magazines.DefaultIfEmpty(new Magazine()).Max(m => m.MediumRating);

        public IEnumerable<Magazine> GetMonthlyMagazines()
        {
            return _magazines.DefaultIfEmpty(new Magazine { Frequency = Frequency.Monthly })
                             .Where(m => m.Frequency == Frequency.Monthly);
        }

        public List<Magazine> RatingGroup(double rating)
        {
            return _magazines.Count == 0 ? null : _magazines.GroupBy(m => m.MediumRating >= rating).Where(g => g.Key == true)
                .SelectMany(g => g).ToList();
        }

        public void AddDefaults()
        {
            for (var i = 0; i < 10; i++)
            {
                _magazines.Add(new Magazine());
            }
        }

        public void AddMagazines(params Magazine[] magazines)
        {
            _magazines.AddRange(magazines);
        }

        public override string ToString()
        {
            var strMagazine = new StringBuilder();
            foreach(var magazine in _magazines)
            {
                strMagazine.Append(magazine.ToString());
                strMagazine.Append(Environment.NewLine);
            }
            return strMagazine.ToString();
        }

        public virtual string ToShortString()
        {
            var strMagazine = new StringBuilder();
            foreach (var magazine in _magazines)
            {
                strMagazine.Append(magazine.ToShortString());
                strMagazine.Append(Environment.NewLine);
            }
            return strMagazine.ToString();
        }

        public void SortByName()
        {
            _magazines.Sort();
        }

        public void SortByDate()
        {
            _magazines.Sort(new Edition());
        }

        public void SortByCirculation()
        {
            _magazines.Sort(new EditionComparer());
        }
    }
}
