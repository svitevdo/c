﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Lab4
{
    public delegate void MagazineListHandler(object source, MagazineListHandlerEventArgs args);

    public class MagazineCollection
    {
        public event MagazineListHandler MagazineAdded;
        public event MagazineListHandler MagazineReplaced;
        public string NameOfCollection { get; set; }

        private readonly List<Magazine> _magazines = new List<Magazine>();

        public double MaxRating => _magazines.DefaultIfEmpty(new Magazine()).Max(m => m.MediumRating);

        public IEnumerable<Magazine> GetMonthlyMagazines()
        {
            return _magazines.DefaultIfEmpty(new Magazine { Frequency = Frequency.Monthly })
                             .Where(m => m.Frequency == Frequency.Monthly);
        }

        public List<Magazine> RatingGroup(double rating)
        {
            return _magazines.Count == 0 ? null : _magazines.GroupBy(m => m.MediumRating >= rating).Where(g => g.Key == true)
                .SelectMany(g => g).ToList();
        }

        public void AddDefaults()
        {
            var handler = MagazineAdded;
            for (var i = 0; i < 10; i++)
            {
                _magazines.Add(new Magazine());
                handler?.Invoke(this, new MagazineListHandlerEventArgs(NameOfCollection, "Added", _magazines.Count));
            }
        }

        public void AddMagazines(params Magazine[] magazines)
        {
            var handler = MagazineAdded;
            foreach (var magazine in magazines)
            {
                _magazines.Add(magazine);
                handler?.Invoke(this, new MagazineListHandlerEventArgs(NameOfCollection, "Added", _magazines.Count));
            }
        }

        public override string ToString()
        {
            var strMagazine = new StringBuilder();
            foreach(var magazine in _magazines)
            {
                strMagazine.Append(magazine.ToString());
                strMagazine.Append(Environment.NewLine);
            }
            return strMagazine.ToString();
        }

        public virtual string ToShortString()
        {
            var strMagazine = new StringBuilder();
            foreach (var magazine in _magazines)
            {
                strMagazine.Append(magazine.ToShortString());
                strMagazine.Append(Environment.NewLine);
            }
            return strMagazine.ToString();
        }

        public void SortByName()
        {
            _magazines.Sort();
        }

        public void SortByDate()
        {
            _magazines.Sort(new Edition());
        }

        public void SortByCirculation()
        {
            _magazines.Sort(new EditionComparer());
        }

        public bool Replace(int index, Magazine magazine)
        {
            if (index < 0 || index >= _magazines.Count)
            {
                return false;
            }
            var handler = MagazineReplaced;
            _magazines[index] = magazine;
            handler?.Invoke(this, new MagazineListHandlerEventArgs(NameOfCollection, "Replaced", index));
            return true;
        }

        public Magazine this[int index]
        {
            get
            {
                if (index < 0 || index >= _magazines.Count)
                {
                    throw new ArgumentOutOfRangeException(nameof(index));
                }
                return _magazines[index];
            }
            set
            {
                if (index < 0 || index >= _magazines.Count)
                {
                    throw new ArgumentOutOfRangeException(nameof(index));
                }
                var handler = MagazineReplaced;
                _magazines[index] = value;
                handler?.Invoke(this, new MagazineListHandlerEventArgs(NameOfCollection, "Replaced", index));
            }
        }
    }
}
