﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Linq;

namespace Lab4
{
    public class Magazine: Edition, IRateAndCopy
    {
        private Frequency _frequency;

        private List<Article> _articles;
        
        private List<Person> _editors;


        public Magazine(string name, Frequency frequency, DateTime publicationDate, List<Article> articles, List<Person> editors, int circulation): base(name, publicationDate, circulation)
        {
            _articles = articles;
            _editors = editors;
            _frequency = frequency;
        }

        public Magazine():base()
        {
            _articles = new List<Article>();
            _editors = new List<Person>();
            _frequency = Frequency.Weekly;

        }

        public Frequency Frequency
        {
            get => _frequency;
            set => _frequency = value;
        }

        public List<Article> Articles
        {
            get => _articles;
            set => _articles = value;
        }

        public List<Person> Editors
        {
            get => _editors;
            set => _editors = value;
        }

        public Edition Edition
        {
            get => new Edition(_name, _publicationDate, _circulation);
            set
            {
                Name = value.Name;
                Circulation = value.Circulation;
                PublicationDate = value.PublicationDate;
            }
        }

        public double MediumRating
        {
            get
            {
               if (_articles.Count == 0)
                    return 0;
                var sum = .0;
                foreach (var article in _articles)
                {
                    sum += ((Article)article).Rating;
                }
                return sum / _articles.Count;

            }
        } 

        public double Rating => MediumRating;

        public void AddArticles(params Article[] articles)
        {
            if (_articles == null)
            {
                _articles = new List<Article>(articles);
            }
            else
            {
                _articles.AddRange(articles);
            }
        }

        public override string ToString()
        {
            var strArticles = new StringBuilder();
            foreach (var article in _articles)
            {
                strArticles.Append($"Article: {article.ToString()}{Environment.NewLine}");
            }
            foreach (var editor in _editors)
            {
                strArticles.Append($"Editor: {editor.ToString()}{Environment.NewLine}");
            }
            return $"Frequency: {_frequency}{Environment.NewLine}" +
                $"Publication date: {_publicationDate}{Environment.NewLine}"+
                $"Edition: {Edition.ToString()}{Environment.NewLine}"
                + $"Articles and Editors: {Environment.NewLine}{strArticles}";
        }

        public virtual string ToShortString() => $"Name: {_name}{Environment.NewLine}"
            + $"Frequency: {_frequency}{Environment.NewLine}"
            + $"Publication date: {_publicationDate}{Environment.NewLine}"
            + $"MediumRating: {MediumRating}";

        public override bool Equals(object obj)
        {
            var magazine = obj as Magazine;
            if (ReferenceEquals(magazine, null))
            {
                return false;
            }
            return Name == magazine.Name &&
                   Frequency == magazine.Frequency &&
                   Circulation == magazine.Circulation &&
                   PublicationDate == magazine.PublicationDate;
        }

        public override int GetHashCode() => HashCode.Combine(Name, Frequency, Circulation, PublicationDate);

        public override object DeepCopy()
        {
            var res = new Magazine(Name, _frequency, PublicationDate, new List<Article>(Articles.Count), new List<Person>(Editors.Count), Circulation);
            foreach(var article in Articles)
            {
                var articleCopy = article as Article;
                res.Articles.Add(articleCopy.DeepCopy() as Article);
            }
            foreach (var editor in Editors)
            {
                var editorCopy = editor as Person;
                res.Editors.Add(editorCopy.DeepCopy() as Person);
            }
            return res;
        }

        public void AddEditors(params Person[] editors)
        {
            if (Editors == null)
            {
                Editors = new List<Person>(editors);
            }
            else
            {
                Editors.AddRange(editors);
            }
        }

        public IEnumerable<Article> GetRatingsMoreThen(double minRating)
        {
            foreach(var o in Articles)
            {
                var article = o as Article;
                if (article == null || article.Rating < minRating)
                {
                    continue;
                }
                yield return article;
            }
        }

        public IEnumerable<Article> GetArticlesWhichInclude(string name)
        {
            foreach (var o in Articles)
            {
                var article = o as Article;
                if (article == null || !article.Name.Contains(name))
                {
                    continue;
                }
                yield return article;
            }
        }

        public static bool operator ==(Magazine magazine1, Magazine magazine2)
        {
            return EqualityComparer<Magazine>.Default.Equals(magazine1, magazine2);
        }

        public static bool operator !=(Magazine magazine1, Magazine magazine2)
        {
            return !(magazine1 == magazine2);
        }

    }
}
