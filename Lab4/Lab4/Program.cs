﻿using System;
using System.Collections.Generic;

namespace Lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            //1
            var collection1 = new MagazineCollection
            {
                NameOfCollection = "collection1"
            };
            var collection2 = new MagazineCollection
            {
                NameOfCollection = "collection2"
            };

            //2
            var l1 = new Listener();
            var l2 = new Listener();
            collection2.AddDefaults();
            collection1.AddDefaults();

            collection1.MagazineAdded += l1.Reporter;
            collection1.MagazineReplaced += l1.Reporter;
            collection2.MagazineAdded += l2.Reporter;
            collection2.MagazineReplaced += l2.Reporter;

            //3
            collection1.AddMagazines(new Magazine(), new Magazine(), new Magazine());
            collection1.Replace(2, new Magazine());
            collection1[1] = new Magazine();

            collection2.AddMagazines(new Magazine());
            collection2.Replace(0, new Magazine());
            collection2[2] = new Magazine();

            Console.WriteLine(l1);
            Console.WriteLine();
            Console.WriteLine(l2);


            Console.ReadKey();
        }
    }
}
