﻿using System;
using System.Text;

namespace Lab3
{
    public interface IRateAndCopy
    {
        double Rating { get; }

        object DeepCopy();
    }
}
