﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1
{
    public class Article
    {
        public Article(Person author, string name, double rating)
        {
            Author = author;
            Name = name;
            Rating = rating;
        }

        public Article()
        {
            Author = new Person();
            Name = "Default";
            Rating = 0;
        }

        public Person Author { get; set; }

        public string Name { get; set; }

        public double Rating { get; set; }

        public override string ToString()
        {
            return $"Author:{Author};Name:{Name};Rating:{Rating}";
        }
    }
}
