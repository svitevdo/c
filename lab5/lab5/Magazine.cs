﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Globalization;

namespace lab5
{
    [Serializable]
    public class Magazine: Edition, IRateAndCopy<Magazine>
    {
        private Frequency _frequency;

        private List<Article> _articles;
        
        private List<Person> _editors;


        public Magazine(string name, Frequency frequency, DateTime publicationDate, List<Article> articles, List<Person> editors, int circulation): base(name, publicationDate, circulation)
        {
            _articles = articles;
            _editors = editors;
            _frequency = frequency;
        }

        public Magazine():base()
        {
            _articles = new List<Article>();
            _editors = new List<Person>();
            _frequency = Frequency.Weekly;

        }

        public Frequency Frequency
        {
            get => _frequency;
            set => _frequency = value;
        }

        public List<Article> Articles
        {
            get => _articles;
            set => _articles = value;
        }

        public List<Person> Editors
        {
            get => _editors;
            set => _editors = value;
        }

        public Edition Edition
        {
            get => new Edition(_name, _publicationDate, _circulation);
            set
            {
                Name = value.Name;
                Circulation = value.Circulation;
                PublicationDate = value.PublicationDate;
            }
        }

        public double MediumRating
        {
            get
            {
               if (_articles.Count == 0)
                    return 0;
                var sum = .0;
                foreach (var article in _articles)
                {
                    sum += ((Article)article).Rating;
                }
                return sum / _articles.Count;

            }
        } 

        public double Rating => MediumRating;

        public void AddArticles(params Article[] articles)
        {
            if (_articles == null)
            {
                _articles = new List<Article>(articles);
            }
            else
            {
                _articles.AddRange(articles);
            }
        }

        public override string ToString()
        {
            var strArticles = new StringBuilder();
            foreach (var article in _articles)
            {
                strArticles.Append($"Article: {article.ToString()}{Environment.NewLine}");
            }
            foreach (var editor in _editors)
            {
                strArticles.Append($"Editor: {editor.ToString()}{Environment.NewLine}");
            }
            return $"Frequency: {_frequency}{Environment.NewLine}" +
                $"Publication date: {_publicationDate}{Environment.NewLine}"+
                $"Edition: {Edition.ToString()}{Environment.NewLine}"
                + $"Articles and Editors: {Environment.NewLine}{strArticles}";
        }

        public virtual string ToShortString() => $"Name: {_name}{Environment.NewLine}"
            + $"Frequency: {_frequency}{Environment.NewLine}"
            + $"Publication date: {_publicationDate}{Environment.NewLine}"
            + $"MediumRating: {MediumRating}";

        public override bool Equals(object obj)
        {
            var magazine = obj as Magazine;
            if (ReferenceEquals(magazine, null))
            {
                return false;
            }
            return Name == magazine.Name &&
                   Frequency == magazine.Frequency &&
                   Circulation == magazine.Circulation &&
                   PublicationDate == magazine.PublicationDate;
        }

        public override int GetHashCode() => HashCode.Combine(Name, Frequency, Circulation, PublicationDate);

        public Magazine DeepCopy()
        {
            Magazine res;
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                res = formatter.Deserialize(stream) as Magazine;
            }
            return res;
        }

        public void AddEditors(params Person[] editors)
        {
            if (Editors == null)
            {
                Editors = new List<Person>(editors);
            }
            else
            {
                Editors.AddRange(editors);
            }
        }

        public IEnumerable<Article> GetRatingsMoreThen(double minRating)
        {
            foreach(var o in Articles)
            {
                var article = o as Article;
                if (article == null || article.Rating < minRating)
                {
                    continue;
                }
                yield return article;
            }
        }

        public IEnumerable<Article> GetArticlesWhichInclude(string name)
        {
            foreach (var o in Articles)
            {
                var article = o as Article;
                if (article == null || !article.Name.Contains(name))
                {
                    continue;
                }
                yield return article;
            }
        }

        public static bool operator ==(Magazine magazine1, Magazine magazine2)
        {
            return EqualityComparer<Magazine>.Default.Equals(magazine1, magazine2);
        }

        public static bool operator !=(Magazine magazine1, Magazine magazine2)
        {
            return !(magazine1 == magazine2);
        }

        public bool Save(string fileName)
        {
            using (var stream = File.Open(fileName, FileMode.Create))
            {
                try
                {
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(stream, this);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public bool Load(string fileName)
        {
            using (var stream = File.Open(fileName, FileMode.Open))
            {
                try
                {
                    var formatter = new BinaryFormatter();
                    var result = formatter.Deserialize(stream) as Magazine;
                    if (result == null)
                    {
                        return false;
                    }
                    Edition = result.Edition;
                    Editors = result.Editors;
                    Frequency = result.Frequency;
                    Articles = result.Articles;
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public bool AddFromConsole()
        {
            try
            {
                Console.WriteLine($"String format:{Environment.NewLine}" +
                    "<article_name>;<author_name> <author_surname> <dd.mm.yyyy date>;<rating.rating>");
                var input = Console.ReadLine().Split(';');
                if (input.Length != 3)
                {
                    return false;
                }
                var personData = input[1].Split(' ');
                if (personData.Length != 3)
                {
                    return false;
                }
                var date = DateTime.ParseExact(personData[2], "dd.mm.yyyy", CultureInfo.InvariantCulture);
                var rating = double.Parse(input[2], CultureInfo.InvariantCulture);
                var author = new Person(personData[0], personData[1], date);
                var article = new Article(author, input[0], rating);
                Articles.Add(article);
                return true;
            }
            catch
            {
                return false;
            }
        }
        //static methods
        public static bool Save(string fileName, Magazine magazine)
        {
            return magazine.Save(fileName);
        }
        public static bool Load(string fileName, Magazine magazine)
        {
            return magazine.Load(fileName);
        }
    }
}
