﻿using System;
using System.Text;

namespace Lab4
{
    public interface IRateAndCopy
    {
        double Rating { get; }

        object DeepCopy();
    }
}
