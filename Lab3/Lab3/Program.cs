﻿using System;
using System.Collections.Generic;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            //1
            var magCollection = new MagazineCollection();
            var time = new DateTime(2015, 12, 25);
            var time1 = new DateTime(2014, 12, 25);
            var time2 = new DateTime(2018, 12, 25);

            var magazine1 = new Magazine("someNew4", Frequency.Yearly, time2, new List<Article>(), new List<Person>(), 10);
            var magazine2 = new Magazine("someNew2", Frequency.Monthly, time, new List<Article>(), new List<Person>(), 1);
            var magazine3 = new Magazine("someNew3", Frequency.Weekly, time1, new List<Article>(), new List<Person>(), 2);
            magazine1.AddArticles(new Article(new Person(), "default1", 10), new Article(new Person(), "default1", 40));
            magazine2.AddArticles(new Article(new Person(), "default2", 50), new Article(new Person(), "default2", 30));
            magazine3.AddArticles(new Article(new Person(), "default3", 60), new Article(new Person(), "default3", 20));

            magCollection.AddMagazines(magazine1, magazine2, magazine3);
            Console.WriteLine(magCollection.ToString());

            //2
            magCollection.SortByName();
            Console.WriteLine("Sort by name");
            Console.WriteLine(magCollection.ToString());

            magCollection.SortByDate();
            Console.WriteLine("Sort by date");
            Console.WriteLine(magCollection.ToString());

            magCollection.SortByCirculation();
            Console.WriteLine("Sort by circulation");
            Console.WriteLine(magCollection.ToString());

            //3

            Console.WriteLine("Max rating");
            Console.WriteLine(magCollection.MaxRating);
            Console.WriteLine(magCollection.ToString());

            Console.WriteLine("Filter");
            var list = magCollection.GetMonthlyMagazines();
            foreach (Magazine magazine in list)
                Console.WriteLine(magazine.ToString());

            Console.WriteLine("Group");
            list = magCollection.RatingGroup(magCollection.MaxRating);
            foreach (Magazine magazine in list)
                Console.WriteLine(magazine.ToString());

            TestCollection test = new TestCollection(1000000);
            test.GetTiming();

            Console.ReadKey();
        }
    }
}
