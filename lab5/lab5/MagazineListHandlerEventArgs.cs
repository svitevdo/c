﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab5
{
    public class MagazineListHandlerEventArgs: EventArgs
    {
        public MagazineListHandlerEventArgs(string collectionName, string type, int elementIndex)
        {
            CollectionName = collectionName;
            Type = type;
            ElementIndex = elementIndex;
        }

        public string CollectionName { get; set; }

        public string Type { get; set; }

        public int ElementIndex { get; set; }

        public override string ToString() => $"Element {ElementIndex} was {Type} in {CollectionName} collection";
    }
}
