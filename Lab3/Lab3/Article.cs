﻿using System;
using System.Text;

namespace Lab3
{
    public class Article: IRateAndCopy
    {
        public Article(Person author, string name, double rating)
        {
            Author = author;
            Name = name;
            Rating = rating;
        }

        public Article()
        {
            Author = new Person();
            Name = "Default";
            Rating = 0;
        }

        public Person Author { get; set; }

        public string Name { get; set; }

        public double Rating { get; set; }

        public override string ToString()
        {
            return $"Author: {Author}; Name: {Name}; Rating: {Rating}";
        }

        /*public override bool Equals(object obj)
        {
            var article = obj as Article;
            if (ReferenceEquals(article, null))
            {
                return false;
            }
            return Author.Equals(article.Author) &&
                   Name == article.Name &&
                   Rating == article.Rating;
        }

        public override int GetHashCode() => HashCode.Combine(Author, Name, Rating);

        public static bool operator ==(Article article1, Article article2)
        {
            return article1.Equals(article2);
        }

        public static bool operator !=(Article article1, Article article2)
        {
            return !article1.Equals(article2);
        }*/

        public virtual object DeepCopy()
        {
            var res = new Article(Author.DeepCopy() as Person, Name, Rating);
            return res;
        }
    }
}
