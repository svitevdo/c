﻿using System;
using System.Text;

namespace lab5
{
    public interface IRateAndCopy<T>
    {
        double Rating { get; }

        T DeepCopy();
    }
}
