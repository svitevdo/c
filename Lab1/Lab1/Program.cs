﻿using System;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            //1
            Magazine magazine = new Magazine();
            Console.WriteLine(magazine.ToShortString());
            //2
            Console.WriteLine($"Weekly:{magazine[Frequency.Weekly]}");
            Console.WriteLine($"Monthly:{magazine[Frequency.Monthly]}");
            Console.WriteLine($"Yearly:{magazine[Frequency.Yearly]}");
            //3
            Article[] articles = new[]
            {
                new Article(),
                new Article(),
                new Article(),
                new Article()
            };
            magazine.Articles = articles;
            magazine.Name = "newMagazine";
            magazine.PublicationDate = DateTime.Now;
            magazine.Frequency = Frequency.Monthly;
            Console.WriteLine(magazine.ToString());
            //4
            magazine.AddArticles(articles);
            Console.WriteLine(magazine.ToString());

            //5
            int rows, columns;
            Console.WriteLine("input count of rows & columns (split by 1 space)");
            string[] input = Console.ReadLine().Split();
            rows = int.Parse(input[0]);
            columns = int.Parse(input[1]);

            var oneDimensional = new Article[rows * columns];
            var twoDimensional = new Article[rows, columns];
            var notched = new Article[rows][];

            for (int i = 0; i < rows; i++)
            {
                notched[i] = new Article[columns];
                for (int j = 0; j < columns; j++)
                {
                    oneDimensional[i * rows + j] = new Article();
                    twoDimensional[i, j] = new Article();
                    notched[i][j] = new Article();
                }
            }

            //for one dimensional array
            var start = Environment.TickCount;

            for (int i = 0; i < rows*columns; i++)
            {
                oneDimensional[i].Name = "default";
            }
            var ticks = Environment.TickCount - start;
            Console.WriteLine($"one dimensional array:{ticks}");

            //for two dimensional array
            start = Environment.TickCount;

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    twoDimensional[i, j].Name = "default";
                }
            }
            ticks = Environment.TickCount - start;
            Console.WriteLine($"two dimensional array:{ticks}");

            //for notched array
            start = Environment.TickCount;

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    notched[i][j].Name = "default";
                }
            }
            ticks = Environment.TickCount - start;
            Console.WriteLine($"notched array:{ticks}");

            Console.ReadKey();
        }
    }
}
