﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab5
{
    public class EditionComparer : IComparer<Edition>
    {
        public int Compare(Edition x, Edition y)
        {
            if (x == null || y==null)
            {
                return -1;
            }
            return x.Circulation.CompareTo(y.Circulation);
        }
    }
}
