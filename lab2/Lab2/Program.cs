﻿using System;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            //1
            var time = DateTime.Now;
            Edition edition1 = new Edition
            {
                PublicationDate = time
            };
            Edition edition2 = new Edition
            {
                PublicationDate = time
            };


            Console.WriteLine($"Equals: {edition1.Equals(edition2)}");
            Console.WriteLine($"Reference: {ReferenceEquals(edition1, edition2)}");
            Console.WriteLine($"HashCode: {edition1.GetHashCode() == edition2.GetHashCode()}");

            //2
            try
            {
                edition1.Circulation = -1000;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            //3
            var magazine = new Magazine();
            var articles = new Article[]
            {
                new Article(),
                new Article()
            };
            var editors = new Person[]
            {
                new Person(),
                new Person()
            };

            magazine.AddArticles(articles);
            magazine.AddEditors(editors);

            Console.WriteLine(magazine);

            //4
            Edition edition = magazine.Edition;
            Console.WriteLine(edition);

            //5
            var copyMagazine = magazine.DeepCopy() as Magazine;

            magazine.Name = "changedName";

            Console.WriteLine($"Original:{magazine.Name}\nCopy:{copyMagazine.Name}");

            //6
            Console.WriteLine("Double: ");
            foreach (var article in magazine.GetRatingsMoreThen(-1))
            {
                Console.WriteLine(article);
            }


            //7
            Console.WriteLine("String: ");
            foreach (var article in magazine.GetArticlesWhichInclude("Def"))
            {
                Console.WriteLine(article);
            }

            Console.ReadKey();
        }
    }
}
