﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab4
{
    public class ListEntry
    {
        public ListEntry(string collectionName, string type, int elementIndex)
        {
            CollectionName = collectionName;
            Type = type;
            ElementIndex = elementIndex;
        }

        public string CollectionName { get; set; }

        public string Type { get; set; }

        public int ElementIndex { get; set; }

        public override string ToString() => $"Element: {ElementIndex}; type: {Type}; collection: {CollectionName}";
    }
}
