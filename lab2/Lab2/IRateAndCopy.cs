﻿using System;
using System.Text;

namespace Lab2
{
    public interface IRateAndCopy
    {
        double Rating { get; }

        object DeepCopy();
    }
}
