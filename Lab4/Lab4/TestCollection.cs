﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace Lab4
{
    public class TestCollection
    {
        private List<Edition> _editions;

        private List<string> _keys;

        private Dictionary<Edition, Magazine> _typeDictionary;

        private Dictionary<string, Magazine> _stringDictionary;

        public Edition this[int index] { get { return _editions[index]; } }

        public TestCollection(int count)
        {
            _editions = new List<Edition>(count);
            _keys = new List<string>(count);
            _typeDictionary = new Dictionary<Edition, Magazine>(count);
            _stringDictionary = new Dictionary<string, Magazine>(count);
        }

        public static Magazine[] GenerateMagazines(int count)
        {
            var res = new Magazine[count];
            for (var i = 0; i < count; i++)
            {
                res[i] = new Magazine();
            }
            return res;
        }

        //methods with time
        public int GetTiming()
        {
            SetDefaults();
            Console.WriteLine("\n--------Timing-------\n");
            Edition edition = this[0];
            var taskBegin = Environment.TickCount;
            _editions.Contains(edition);
            var taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for first: {0}", taskEnd - taskBegin);

            edition = this[1];
            taskBegin = Environment.TickCount;
            _editions.Contains(edition);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for second: {0}", taskEnd - taskBegin);

            edition = this[2];
            taskBegin = Environment.TickCount;
            _editions.Contains(edition);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for third: {0}", taskEnd - taskBegin);

            edition = this[3];
            taskBegin = Environment.TickCount;
            _editions.Contains(edition);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for fourth: {0}", taskEnd - taskBegin);


            Console.WriteLine("\n\n----Dictionary Keys----\n");
            Edition[] keys = new Edition[4];
            _typeDictionary.Keys.CopyTo(keys, 0);

            taskBegin = Environment.TickCount;
            _typeDictionary.ContainsKey(keys[0]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for first: {0}", taskEnd - taskBegin);

            taskBegin = Environment.TickCount;
            _typeDictionary.ContainsKey(keys[1]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for second: {0}", taskEnd - taskBegin);

            taskBegin = Environment.TickCount;
            _typeDictionary.ContainsKey(keys[2]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for third: {0}", taskEnd - taskBegin);

            taskBegin = Environment.TickCount;
            _typeDictionary.ContainsKey(keys[3]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for fourth: {0}", taskEnd - taskBegin);


            Console.WriteLine("\n\n----Dictionary Values----\n");
            Magazine[] magazines = new Magazine[4];
            _typeDictionary.Values.CopyTo(magazines, 0);

            taskBegin = Environment.TickCount;
            _typeDictionary.ContainsValue(magazines[0]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for first: {0}", taskEnd - taskBegin);

            taskBegin = Environment.TickCount;
            _typeDictionary.ContainsValue(magazines[1]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for second: {0}", taskEnd - taskBegin);

            taskBegin = Environment.TickCount;
            _typeDictionary.ContainsValue(magazines[2]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for third: {0}", taskEnd - taskBegin);

            taskBegin = Environment.TickCount;
            _typeDictionary.ContainsValue(magazines[3]);
            taskEnd = Environment.TickCount;
            Console.WriteLine("\nSearch for fourth: {0}", taskEnd - taskBegin);

            return 0;
        }

        private void SetDefaults()
        {
            _editions.Add(new Edition("Edition1", new DateTime(2015, 12, 25), 1));
            _editions.Add(new Edition("Edition2", new DateTime(2015, 12, 25), 2));
            _editions.Add(new Edition("Edition3", new DateTime(2015, 12, 25), 3));
            _editions.Add(new Edition());

            var time = new DateTime(2015, 12, 25);
            var time1 = new DateTime(2014, 12, 25);
            var time2 = new DateTime(2018, 12, 25);

            var magazine1 = new Magazine("someNew4", Frequency.Yearly, time2, new List<Article>(), new List<Person>(), 10);
            var magazine2 = new Magazine("someNew2", Frequency.Monthly, time, new List<Article>(), new List<Person>(), 1);
            var magazine3 = new Magazine("someNew3", Frequency.Weekly, time1, new List<Article>(), new List<Person>(), 2);

            _typeDictionary.Add(_editions[0], magazine1);
            _typeDictionary.Add(_editions[1], magazine2);
            _typeDictionary.Add(_editions[2], magazine3);
            _typeDictionary.Add(new Edition(), new Magazine());
        }
    }
}
